use dbTaskTitans
go

	create table arma (
	id_arma int primary key identity,
	nome_arma varchar(30) not null,
	intarma int not null,
	forarma int not null,
	sabarma int not null,
	desarma int not null,
	constarma int not null,
	carisarma int not null,
	);
	go

	alter table arma add fk_bag int
	go
	alter table arma add fk_tipoarma int 
	go

		create table bag (
	id_bag int primary key identity,
	fk_arma int,
	fk_armor int,
	fk_acess int,
	fk_person int
	);
	go







	alter table bag
	add constraint fk_arma
	foreign key (fk_arma)
	references bag_weapon(id_bag_weapon)
	go
	
	alter table arma
	add constraint fk_bag
	foreign key (fk_bag)
	references bag_weapon(id_bag_weapon)
	go





			create table bag_armor (
	id_bag_armor int primary key identity,
	);
	go




	alter table bag
	add constraint fk_armor
	foreign key (fk_armor)
	references bag_armor(id_bag_armor)
	go




	alter table armor drop column fk_bag
	go
	alter table armor add fk_bagarmor int
	go

	alter table armor
	add constraint fk_bagarmor
	foreign key (fk_bagarmor)
	references bag_armor(id_bag_armor)
	go


	create table bag_acess (
	id_bag_acess int primary key identity,
	);
	go

	alter table bag
	add constraint fk_acess
	foreign key (fk_acess)
	references bag_acess(id_bag_acess)
	go

	alter table acessorio add fk_bagacess int
	go
	
	alter table acessorio
	add constraint fk_bagacess
	foreign key (fk_bagacess)
	references bag_acess(id_bag_acess)
	go




	/*ta sussa ^^^^^*/






	create table usuario (
	id_user int primary key identity,
	login varchar(20)  not null,
	nome varchar(20) not null,
	fk_turma int not null,
	email varchar(100) not null,
	nascimento varchar(8) not null,
	senha varchar(20) not null,
	cpf varchar(11) not null unique
	);
	go

		alter table usuario drop column fk_turma
	alter table usuario add fk_inst int

		alter table usuario add fk_id_bag int


	create table personagem (
	id_person int identity primary key,
	nome varchar(20) not null,
	cla varchar(15) not null unique,
	fk_armor int not null,
	fk_arma int not null,
	vida int not null,
	xp bigint,
	nivel int null,
	intel int not null,
	forc int not null,
	sab int not null,
	des int not null,
	const int not null,
	caris int not null
	);
	go

		alter table personagem add fk_class int not null;
	alter table personagem add pa int not null;

		alter table personagem add fk_user int
	go


	Alter table personagem 
	add constraint fk_user
	foreign key (fk_user)
	references usuario(id_user);
	go


	/* ta tussa */ 





		create table armor (
	id_armor int primary key identity,
	nome_armor varchar(30) not null,
	intarmor int not null,
	forarmor int not null,
	sabarmor int not null,
	desarmor int not null,
	constarmor int not null,
	carisarmor int not null,
	fk_bag int,
						);
	go

	create table desafio(
	id_desafio int not null primary key identity,
	descricao varchar(1000) not null,
	xddf int not null,
	fk_armordf int,
	fk_armadf int,
	fk_tipodesafio int,
	);
	go

	alter table desafio add fk_acess int
	go


	create table tipodesafio (
	id_desafio int primary key identity,
	nomedf varchar(30),
	);

	alter table desafio
	add constraint fk_tipodesafio
	foreign key (fk_tipodesafio)
	references tipodesafio(id_desafio)
	go

	/* ta sussa ^^^^ */ 

		create table class (
	id_class int identity,
	nome_class varchar(20),
	intclass int not null,
	sabclass int not null,
	forclass int not null,
	desclass int not null,
	carclass int not null,
	conclass int not null,
	fk_tipoClass int not null,
	);
	go

		create table tipoClass (
	id_class int identity,
	alcance varchar(20)
	); 
	go

		create table tipoarma (
	id_tipo_arma int primary key identity,
	nome_tipo_arma varchar(20) not null,
	);
	go


	/* ta sussa ^^^^^^^^^^ */ 



		create table instituicao (
	id_inst int primary key identity,
	fk_turma varchar(15)
	);
	go


		Create table acessorio (
	id_acess int primary key identity,
	nome_acess varchar(30) not null,
	intacess int not null,
	sabacess int not null,
	desacess int not null,
	foracess int not null,
	conacess int not null,
	caracess int not null,
	);

			alter table bag
	add constraint fk_person
	foreign key (fk_person)
	references personagem(id_person)
	go


	/* sucesso at� aqui   */ 


	alter table usuario drop column fk_id_bag
	go

	alter table arma drop column fk_tipoarma
	go

	
	alter table tipoarma add fk_armat int
	go

	alter table tipoarma
	add constraint fk_armat
	foreign key (fk_armat)
	references arma(id_arma)
	go

	/* sucesso ^^^^^^^^^^ */ 


	create table des_per (
	id_des_per int primary key identity,
	);
	go

	alter table desafio add fk_persond int
	go

	alter table desafio
	add constraint fk_persond
	foreign key (fk_persond)
	references des_per(id_des_per)
	go

	alter table personagem add fk_desaf int
	go

	alter table personagem
	add constraint fk_desaf
	foreign key (fk_desaf)
	references des_per(id_des_per)
	go

	/* sucesso ^^^^^^^^^^ */ 


	create table ace_des (
	id_ace_des int primary key identity,
	);
	go

	alter table desafio add fk_aced int
	go

	alter table desafio
	add constraint fk_aced
	foreign key (fk_aced)
	references ace_des(id_ace_des)
	go

	alter table acessorio add fk_desaa int
	go

	alter table acessorio
	add constraint fk_desaa
	foreign key (fk_desaa)
	references ace_des(id_ace_des)
	go





	/* sucesso ^^^^^^^^^^ */



	create table arm_des (
	id_arm_des int primary key identity,
	);
	go

	

	alter table desafio add fk_armd int
	go

	alter table desafio
	add constraint fk_armd
	foreign key (fk_armd)
	references arm_des(id_arm_des)
	go

	alter table arma add fk_desaar int
	go

	alter table arma
	add constraint fk_desaar
	foreign key (fk_desaar)
	references arm_des(id_arm_des)
	go


	/* sucesso ^^^^^^^ */ 


		create table armor_des (
	id_armor_des int primary key identity,
	);
	go

	

	alter table desafio add fk_armord int
	go

	alter table desafio
	add constraint fk_armord
	foreign key (fk_armord)
	references armor_des(id_armor_des)
	go

	alter table armor add fk_desaarmor int
	go

	alter table armor
	add constraint fk_desaarmor
	foreign key (fk_desaarmor)
	references armor_des(id_armor_des)
	go

	/* sucesso ^^^^^^^^^^ */ 

	alter table class drop column fk_tipoClass
	go

	alter table tipoClass add fk_classt int
	go

	alter table class drop column id_class
	alter table class add id_class int primary key identity
	
	alter table tipoClass
	add constraint fk_classt
	foreign key (fk_classt)
	references class(id_class)
	go	

	alter table personagem
	add constraint fk_class
	foreign key (fk_class)
	references class(id_class)
	go

	/* sucesso ^^^^^^^^^^ */ 

	alter table instituicao add fk_useri int
	go

	alter table instituicao
	add constraint fk_useri
	foreign key (fk_useri)
	references usuario(id_user)
	go

	/* sucesso ^^^^^^^^^^ */ 


	create table turma (
	id_turma int primary key identity,
	nome_turma varchar(15)
	);

	alter table instituicao drop column fk_turma
	alter table instituicao add fk_turma int
	go

	alter table instituicao
	add constraint fk_turma
	foreign key (fk_turma)
	references turma(id_turma)
	go